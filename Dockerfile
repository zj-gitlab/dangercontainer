FROM ruby:alpine
MAINTAINER Z.J. van de Weg <dangercontainer@zjvandeweg.nl>

RUN apk update && apk upgrade && apk add --no-cache git
RUN gem install danger-gitlab --no-document
